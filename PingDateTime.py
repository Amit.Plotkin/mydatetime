import requests
from datetime import datetime
import sqlite3

conn = sqlite3.connect('Database/testDB.db')

r = requests.get('http://127.0.0.1:5000/')

date = datetime.now()

f = open(f"html/Index{str(date.timestamp())}.html", "w")
f.write(str(r.content))
f.close()

sqlite_insert_query ="INSERT INTO DatesTable  (Body,Date) VALUES  (?,?)"

cursor = conn.cursor()


count = cursor.execute(sqlite_insert_query,(str(r.content),date))
conn.commit()
print("Record inserted successfully into SqliteDb_developers table ", cursor.rowcount)
cursor.close()