from flask import Flask
from datetime import datetime


app = Flask(__name__)


@app.route('/')
def hello_world():
    date = datetime.now()
    return str(date)


if __name__ == '__main__':
    app.run()